import * as React from 'react';
import { Md5 } from 'ts-md5/dist/md5';
import { IACSConfig } from './types/IACSConfig';
import { IACS } from './types/IACS';
import { IACSItem } from './types/IACSItem';

export const ACSContext = React.createContext<IACS | undefined>(undefined);

const ACSProvider = function<P>(
  Component: string | React.ComponentClass<any, any> | React.FunctionComponent<any>,
  acsConfig: IACSConfig
) {
  const data: IACS = {
    /**
     * configuration
     */
    acsConfig,

    /**
     * cache data for a faster lookup register
     */
    cache: {},

    /**
     * gets the function
     *
     * @param identifier {string}
     */
    getFunc(identifier: string): any {
      return this.cache[identifier];
    },

    /**
     * set the function to the cache
     *
     * @param rule {string}
     * @param identifier {string}
     */
    setFunc(rule: string, identifier: string): any {
      this.cache[identifier] = this.acsConfig.rules[rule];
      return this.cache[identifier];
    },

    /**
     * find rule function for given identifier
     *
     * @param identifier {string | object}
     */
    find(identifier: string | object): any {
      const cacheID = String(Md5.hashStr(JSON.stringify(identifier)));
      const cacheFunc = this.getFunc(cacheID);

      if (cacheFunc) {
        return cacheFunc;
      } else if (this.acsConfig) {
        const acsItem: IACSItem | undefined = this.acsConfig.data.find(item => this.acsConfig.find(item, identifier));
        if (acsItem) {
          return this.setFunc(acsItem.rule, cacheID);
        }

        if (this.acsConfig.rules.default) {
          return this.setFunc('default', cacheID);
        }
      }

      return (props: any) => props;
    }
  };

  return (props: P) => (
    <ACSContext.Provider value={data}>
      <Component {...props} />
    </ACSContext.Provider>
  );
};

export default ACSProvider;
