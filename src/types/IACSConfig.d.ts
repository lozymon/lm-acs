import { IACSItem } from './IACSItem';

export interface IACSConfig {
  rules: { [key: string]: (props: any) => any };
  find: (item: IACSItem, identifier: string | object) => boolean;
  data: Array<IACSItem>;
}
