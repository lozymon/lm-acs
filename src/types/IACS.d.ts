import { IACSConfig } from './IACSConfig';

export interface IACS {
  cache: { [key: string]: any };
  getFunc: (identifier: string | object) => any;
  setFunc: (rule: string, identifier: string | object) => any;
  acsConfig: IACSConfig;
  find: (name: string | object) => any;
}
