import * as React from 'react';
import { IACS } from './types/IACS';
import { ACSContext } from './ACSProvider';

/**
 *
 * @param Component {string | React.ComponentClass<any, any> | React.FunctionComponent<any>}
 * @param identifier {string | object | Function}
 */
const ACSConsumer = function<P>(
  Component: string | React.ComponentClass<any, any> | React.FunctionComponent<any>,
  identifier: string | object | Function
) {
  return (props: P) => {
    return (
      <ACSContext.Consumer>
        {(context: IACS) => {
          const func = typeof identifier !== 'function' ? context.find(identifier) : context.find(identifier(props));
          let newProps = typeof func === 'function' ? func(props) : props;
          return <Component {...newProps} />;
        }}
      </ACSContext.Consumer>
    );
  };
};

export default ACSConsumer;
