# acs

>

[![NPM](https://img.shields.io/npm/v/acs.svg)](https://www.npmjs.com/package/acs) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save lm-acs
```

## License

MIT © [Kim Andre Furevikstrand](https://gitlab.com/lozymon)
