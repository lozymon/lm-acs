export default {
  B: props => ({
    ...props,
    disabled: true,
    value: '',
    onChange: undefined
  }),
  R: props => ({
    ...props,
    disabled: true,
    onChange: undefined
  }),
  W: props => ({ ...props }),
  default: props => props
};
