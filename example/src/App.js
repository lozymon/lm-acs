import React, { Component } from 'react';

import { ACSProvider } from 'acs';
import Input from './Input';
import acsData from './ACSConfig/acsData';
import acsFind from './ACSConfig/acsFind';
import acsRules from './ACSConfig/acsRules';

class App extends Component {
  state = {
    test1: 'test1',
    test2: 'test2',
    test3: 'test3'
  };

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  render() {
    return (
      <div>
        <p>Controls the props access to every component.</p>
        <Input label='test1' name='test1' value={this.state.test} onChange={this.onChange} />
        <Input label='test2' name='test2' value={this.state.test2} onChange={this.onChange} />
        <Input label='test3' name='test3' value={this.state.test3} onChange={this.onChange} />
      </div>
    );
  }
}

export default ACSProvider(App, {
  data: acsData,
  find: acsFind,
  rules: acsRules
});
