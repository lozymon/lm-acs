import React, { Component } from 'react';
import { ACSConsumer } from 'acs';

class Input extends Component {
  onChange = event => {
    const { name, onChange } = this.props;
    onChange && onChange(name, event.target.value);
  };

  render() {
    const { label, name, value, disabled } = this.props;
    return (
      <div>
        <label htmlFor={name}>
          {label}
          <input name={name} id={name} value={value} disabled={disabled} onChange={this.onChange} />
        </label>
      </div>
    );
  }
}

export default ACSConsumer(Input, props => props.name);
